Dockerized Indico
=================

This is a dockerized indico `Indico <https://docs.getindico.io>`_ instance.

Preparation
-----------

- Clone or download the repository.

- Enter the directory: ``cd docker-indico``

- Change the password and adjust the file content to your server
  configuration in *docker-compose.yml* file.

Running the Server
------------------

When preparation has been completed successfully, the container can be started
and stopped at any time.

When running for the first time, a bootstrap will be done to create the
superuser.

To start the container, issue: ``docker-compose up``.
This runs it in interactive mode with the log output printed on screen. Stop
it with Ctr-C.

To run container in daemon mode, issue: ``docker-compose up -d``.

The website is accessible at `http://localhost:8000 <http://localhost:8000>`_

Import/Export of Indico Events
------------------------------

To facilitate the migration of events from one server to another, Indico
provides an easy to use utility function that dumps/loads the database content
to/from single files.

To import events:
- Copy the ``.ind`` file into the Indico container volume (eg. file *1.ind*):
  ``docker cp 1.ind docker-indico_indico_1:/opt/indico``
- Import the event:
  ``docker-compose exec indico indico event import /opt/indico/1.ind``
- Remove the file from the container volume:
  ``docker-compose exec indico rm /opt/indico/1.ind``

To export events:
- To export an event (eg. file *1.ind*):
  ``docker-compose exec indico indico event export 1 /opt/indico/1.ind``
- Copy the file out of the Indico container volume:
  ``docker cp docker-indico_indico_1:/opt/indico/1.ind .``
- Remove the file from the container volume:
  ``docker-compose exec indico rm /opt/indico/1.ind``
