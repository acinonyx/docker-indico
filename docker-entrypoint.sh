#!/bin/sh -e
#
# Indico Docker startup script
#
# Copyright (C) 2020, 2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

INDICO_HOME_DIR="/opt/indico"
INDICO_CONFIG_DIR="/opt/indico/etc"
CONFIG_VARS="ATTACHMENT_STORAGE:str
AUTH_PROVIDERS:dict
BASE_URL:str
CACHE_DIR:str
CATEGORY_CLEANUP:dict
CELERY_BROKER:str
CELERY_CONFIG:dict
CELERY_RESULT_BACKEND:str
CHECKIN_APP_URL:str
COMMUNITY_HUB_URL:str
CUSTOMIZATION_DEBUG:bool
CUSTOMIZATION_DIR:str
CUSTOM_COUNTRIES:dict
CUSTOM_LANGUAGES:dict
DB_LOG:bool
DEBUG:bool
DEFAULT_LOCALE:str
DEFAULT_TIMEZONE:str
DISABLE_CELERY_CHECK:int
EMAIL_BACKEND:str
ENABLE_APPLE_WALLET:bool
ENABLE_GOOGLE_WALLET:bool
ENABLE_ROOMBOOKING:bool
EXPERIMENTAL_EDITING_SERVICE:bool
EXTERNAL_REGISTRATION_URL:str
FAILED_LOGIN_RATE_LIMIT:str
FAVICON_URL:str
HELP_URL:str
IDENTITY_PROVIDERS:dict
LOCAL_GROUPS:bool
LOCAL_IDENTITIES:bool
LOCAL_MODERATION:bool
LOCAL_REGISTRATION:bool
LOGGING_CONFIG_FILE:str
LOGIN_LOGO_URL:str
LOGO_URL:str
LOG_DIR:str
MAX_DATA_EXPORT_SIZE:int
MAX_UPLOAD_FILES_TOTAL_SIZE:int
MAX_UPLOAD_FILE_SIZE:int
NO_REPLY_EMAIL:str
PLUGINS:list
PROFILE:bool
PROVIDER_MAP:dict
PUBLIC_SUPPORT_EMAIL:str
REDIS_CACHE_URL:str
ROUTE_OLD_URLS:bool
SCHEDULED_TASK_OVERRIDE:dict
SECRET_KEY:str
SENTRY_DSN:str
SENTRY_LOGGING_LEVEL:str
SESSION_LIFETIME:int
SIGNUP_CAPTCHA:bool
SIGNUP_RATE_LIMIT:str
SMTP_ALLOWED_SENDERS:list
SMTP_CERTFILE:str
SMTP_KEYFILE:str
SMTP_LOGIN:str
SMTP_PASSWORD:str
SMTP_SENDER_FALLBACK:str
SMTP_SERVER:tuple
SMTP_TIMEOUT:int
SMTP_USE_CELERY:bool
SMTP_USE_TLS:bool
SQLALCHEMY_DATABASE_URI:str
SQLALCHEMY_MAX_OVERFLOW:int
SQLALCHEMY_POOL_RECYCLE:int
SQLALCHEMY_POOL_SIZE:int
SQLALCHEMY_POOL_TIMEOUT:int
STATIC_FILE_METHOD:dict
STATIC_SITE_STORAGE:str
STORAGE_BACKENDS:dict
STRICT_LATEX:bool
SUPPORT_EMAIL:str
SYSTEM_NOTICES_URL:str
TEMP_DIR:str
USE_PROXY:bool
WALLET_LOGO_URL:str
WORKER_NAME:str
XELATEX_PATH:str"

create_config() {
	for config_var in $1; do
		_var="${config_var%%:*}"
		_type="${config_var#*:}"
		_value=$(eval "echo \$$_var")
		if [ -n "$_value" ]; then
			case $_type in
				str)
					echo "${_var} = '${_value}'"
					;;
				int|bool|list|dict|tuple)
					echo "${_var} = ${_value}"
					;;
			esac
		fi
	done
}

prepare_database() {
	[ -f "${INDICO_HOME_DIR}/.installed-docker" ] || indico db prepare
	indico db upgrade
	indico db --all-plugins upgrade
}

prepare_paths() {
	python_site_packages_dir="$(pip show --no-cache-dir indico | awk '/^Location: / { print $2 }')"
	install -d \
		"${INDICO_CONFIG_DIR}" \
		"${INDICO_HOME_DIR}/archive" \
		"${INDICO_HOME_DIR}/cache" \
		"${INDICO_HOME_DIR}/tmp" \
		"${INDICO_HOME_DIR}/log" \
		"${INDICO_HOME_DIR}/web/static"
	cp -dr "${python_site_packages_dir}/indico/logging.yaml.sample" "${INDICO_CONFIG_DIR}/logging.yaml"
	cp -dr "${python_site_packages_dir}/indico/web/indico.wsgi" "${INDICO_HOME_DIR}/web/"
	cp -dr "${python_site_packages_dir}/indico/web/static" "${INDICO_HOME_DIR}/web/"
}

set_uwsgi_defaults() {
	export UWSGI_AUTO_PROCNAME="${UWSGI_AUTO_PROCNAME:-true}"
	export UWSGI_BUFFER_SIZE="${UWSGI_BUFFER_SIZE:-20480}"
	export UWSGI_CHMOD_SOCKET="${UWSGI_CHMOD_SOCKET:-770}"
	export UWSGI_CHOWN_SOCKET="${UWSGI_CHOWN_SOCKET:-indico:www-data}"
	export UWSGI_DISABLE_LOGGING="${UWSGI_DISABLE_LOGGING:-true}"
	export UWSGI_ENABLE_THREADS="${UWSGI_ENABLE_THREADS:-true}"
	export UWSGI_EVIL_RELOAD_ON_RSS="${UWSGI_EVIL_RELOAD_ON_RSS:-8192}"
	export UWSGI_GID="${UWSGI_GID:-www-data}"
	export UWSGI_HARAKIRI="${UWSGI_HARAKIRI:-900}"
	export UWSGI_HARAKIRI_VERBOSE="${UWSGI_HARAKIRI_VERBOSE:-true}"
	export UWSGI_MASTER="${UWSGI_MASTER:-true}"
	export UWSGI_MAX_REQUESTS="${UWSGI_MAX_REQUESTS:-2500}"
	export UWSGI_MEMORY_REPORT="${UWSGI_MEMORY_REPORT:-true}"
	export UWSGI_PROCESSES="${UWSGI_PROCESSES:-4}"
	export UWSGI_PROCNAME_PREFIX_SPACED="${UWSGI_PROCNAME_PREFIX_SPACED:-indico}"
	export UWSGI_PROTOCOL="${UWSGI_PROTOCOL:-uwsgi}"
	export UWSGI_RELOAD_ON_RSS="${UWSGI_RELOAD_ON_RSS:-2048}"
	export UWSGI_SINGLE_INTERPRETER="${UWSGI_SINGLE_INTERPRETER:-true}"
	export UWSGI_SOCKET="${UWSGI_SOCKET:-:3031}"
	export UWSGI_STATS="${UWSGI_STATS:-:1717}"
	export UWSGI_TOUCH_RELOAD="${UWSGI_TOUCH_RELOAD:-${INDICO_HOME_DIR}/web/indico.wsgi}"
	export UWSGI_UID="${UWSGI_UID:-indico}"
	export UWSGI_UMASK="${UWSGI_UMASK:-027}"
	export UWSGI_VACUUM="${UWSGI_VACUUM:-true}"
	export UWSGI_WSGI_FILE="${UWSGI_WSGI_FILE:-${INDICO_HOME_DIR}/web/indico.wsgi}"
}

export INDICO_CONFIG="${INDICO_CONFIG_DIR}/indico.conf"

case "$1" in
	uwsgi)
		prepare_paths
		create_config "$CONFIG_VARS" > "${INDICO_CONFIG}"
		prepare_database
		set_uwsgi_defaults
		touch "${INDICO_HOME_DIR}/.installed-docker"
		;;
	celery)
		if [ ! -f "${INDICO_HOME_DIR}/.installed-docker" ]; then
			echo "Indico not installed yet! Exiting..." >&2
			exit 1
		fi
		shift
		set -- indico celery worker "${@:--B}"
		;;
esac

exec "$@"
