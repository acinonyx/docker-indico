# Indico Docker image
#
# Copyright (C) 2018-2020 Artur Scholz <artur.scholz@librecube.net>
# Copyright (C) 2020, 2024 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PYTHON_IMAGE_TAG=3.12.6-bookworm
FROM python:${PYTHON_IMAGE_TAG}
LABEL org.opencontainers.image.authors='Vasilis Tsiligiannis <acinonyx@openwrt.gr>'

ARG INDICO_VERSION=3.3.4
ARG INDICO_UID=999
ARG INDICO_HOME_DIR=/opt/indico
ARG UWSGI_VERSION=2.0.26
ARG INDICO_PLUGINS_VERSION=3.3.1

# Disable Python compilation to bytecode
ENV PYTHONDONTWRITEBYTECODE=1

# Force the stdout and stderr streams to be unbuffered
ENV PYTHONUNBUFFERED=1

# Create 'indico' user and group
RUN groupadd -r -g ${INDICO_UID} indico \
	&& useradd -r -u ${INDICO_UID} \
		-g indico \
		-d ${INDICO_HOME_DIR} \
		-s /sbin/nologin \
		indico

# Prepare folders
RUN install -o ${INDICO_UID} -g ${INDICO_UID} -d ${INDICO_HOME_DIR} \
	&& install -o ${INDICO_UID} -g ${INDICO_UID} -d ${INDICO_HOME_DIR}/etc

# Install system packages
RUN apt-get update \
	&& apt-get install -qy --no-install-recommends \
		texlive-xetex \
		texlive-fonts-recommended \
	&& rm -r /var/lib/apt/lists/*

# Install 'uwsgi' and 'indico'
RUN pip install --no-cache-dir \
	setuptools \
	uwsgi==${UWSGI_VERSION} \
	indico==${INDICO_VERSION} \
	indico-plugins==${INDICO_PLUGINS_VERSION}

VOLUME ["${INDICO_HOME_DIR}"]

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

# Expose uWSGI ports
EXPOSE 3031
EXPOSE 1717

# Run application
CMD ["uwsgi"]
